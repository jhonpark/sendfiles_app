import axios from 'axios';

const api = axios.create({
    baseURL: 'https://backendjohn.herokuapp.com'
});

export default api;