import React, { Component } from 'react';
import { StyleSheet, Platform } from "react-native";
import {
    getBottomSpace,
    getStatusBarHeight
} from "react-native-iphone-x-helper";
import { distanceInWords } from 'date-fns';
//import ImagePicker from 'react-native-image-crop-picker';
import RNFS from 'react-native-fs';
import fileView from 'react-native-file-viewer';
import pt from "date-fns/locale/pt"
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import storage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-picker';
import socket from 'socket.io-client';

import api from '../services/api';


export default class Files extends Component {
    state = {
        boxes: {},
    }

    async componentDidMount() {
        const id = await storage.getItem('@rocketBox:box');
        this.subscribeToNewFiles(id);
        const response = await api.get(`boxes/${id}`);
        console.log(response.data)
        this.setState({ boxes: response.data });
    }

    handleUpload = () => {
        ImagePicker.launchImageLibrary({}, async response => {
            console.log(this.state.boxes._id)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else {
                const data = new FormData;

                const [prefix, suffix] = response.fileName.split('.');
                const ext = suffix.toLocaleLowerCase() === 'heic' ? 'jpg' : suffix;

                data.append("file", {
                    uri: response.uri,
                    type: response.type,
                    name: `${prefix}.${ext}`
                })
                api.post(`boxes/${this.state.boxes._id}/files`, data);
            }
        });
    };

    subscribeToNewFiles = id => {
        const io = socket("https://backendjohn.herokuapp.com");
        io.emit("connection", id);

        io.on("file", data => {
            this.setState({
                boxes: 
                { 
                    ...this.state.boxes, 
                    files: [data, ...this.state.boxes.files]
                }
            });
        });
    }

    openFile = async file => {
        try {
            const filePath = `${RNFS.DocumentDirectoryPath}/${file.title}`;
            await RNFS.downloadFile({
                fromUrl: file.url,
                toFile: filePath
            })
            await fileView.open(filePath);
        } catch (err) {
            console.log('nao suportado');
        }
    }

    renderItem = ({ item }) => (
        <TouchableOpacity
            onPress={() => this.openFile(item)}
            style={styles.file}
        >
            <View style={styles.fileInfo}>
                <Icon name="insert-drive-file" size={24} color='#A5CFFF' />
                <Text style={styles.fileTitle}>{item.title}</Text>
                <Text style={styles.fileDate}>{distanceInWords(
                    item.createdAt, new Date(), {
                        locale: pt
                    })}
                </Text>
            </View>
        </TouchableOpacity>
    )

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.boxTitle}>{this.state.boxes.title}</Text>
                <FlatList
                    data={this.state.boxes.files}
                    keyExtractor={key => key._id}
                    style={styles.list}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    renderItem={this.renderItem}

                />
                <TouchableOpacity onPress={() => { this.handleUpload() }} style={styles.fab}>
                    <Icon name='cloud-upload' size={24} color='#FFF' />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingTop: Platform.OS === "ios" ? getStatusBarHeight() : 0,
        flex: 1
    },

    boxTitle: {
        marginTop: 50,
        textAlign: "center",
        fontSize: 24,
        fontWeight: "bold",
        color: "#333"
    },

    list: {
        marginTop: 30
    },

    file: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingVertical: 20
    },

    separator: {
        height: 1,
        backgroundColor: "#EEE"
    },

    fileInfo: {
        flexDirection: "row",
        width: '70%'
    },

    fileTitle: {
        fontSize: 12,
        color: "#333",
        marginLeft: 8
    },

    fileDate: {
        fontSize: 14,
        color: "#666"
    },

    fab: {
        position: "absolute",
        right: 30,
        bottom: 30 + getBottomSpace(),
        width: 60,
        height: 60,
        backgroundColor: "#7159c1",
        borderRadius: 30,
        alignItems: "center",
        justifyContent: "center"
    }
});