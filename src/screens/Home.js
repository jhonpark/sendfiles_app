import React, { Component } from 'react';
import api from '../services/api';
import { View, Image, TextInput, TouchableOpacity, Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


const logo = require('../assets/logo.png');

class Home extends Component {
    state = {
        newBoxes: ''
    }
    async componentDidMount() {
        const box = await AsyncStorage.getItem("@rocketBox:box");
        if (box) {
            this.props.navigation.navigate('Files');
        }

    }

    handleBoxes = async () => {
        const response = await api.post("boxes", {
            title: this.state.newBoxes
        })

        console.log(response.data)
        await AsyncStorage.setItem("@rocketBox:box", response.data._id);
        this.props.navigation.navigate('Files');

    }

    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.logo} source={logo} />
                <TextInput
                    style={styles.input}
                    value={this.state.newBoxes}
                    onChangeText={value => { this.setState({ newBoxes: value }) }}
                    placeholder='Criar um boxe'
                    placeholderTextColor='#999'
                    autoCapitalize='none'
                    autoCorrect={false}
                    underlineColorAndroid='transparent'
                />
                <TouchableOpacity onPress={() => this.handleBoxes()} style={styles.button}>
                    <Text style={styles.txtButton}>Criar</Text>
                </TouchableOpacity>
            </View>
        )
    }
}


const styles = {
    container: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'center',
        paddingHorizontal: 30
    },
    logo: {
        alignSelf: 'center'
    },
    input: {
        height: 48,
        borderWidth: 1,
        borderColor: '#DDD',
        borderRadius: 4,
        fontSize: 16,
        paddingHorizontal: 20,
        marginTop: 30
    },
    button: {
        height: 48,
        borderRadius: 4,
        fontSize: 16,
        paddingHorizontal: 20,
        marginTop: 10,
        backgroundColor: '#7159c1',
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtButton: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#FFF'
    }

}

export default Home;