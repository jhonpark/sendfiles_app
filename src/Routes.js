import { createSwitchNavigator, createAppContainer } from 'react-navigation'

import Home from './screens/Home';
import Files from './screens/Files';

const Routes = createAppContainer(
    createSwitchNavigator({
        Home,
        Files
    })
)

export default Routes;
